package com.groupeisi.m2gl.sqrtbillngono;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CalculationControllerTests {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void sqrtEndpointReturnsCorrectResult() throws Exception {
        this.mockMvc.perform(post("/sqrt")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"number\": 4}"))
                .andExpect(status().isOk())
                .andExpect(content().string("2.0"));
    }

    @Test
    void sqrtEndpointReturnsErrorForNegativeNumber() throws Exception {
        this.mockMvc.perform(post("/sqrt")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"number\": -1}"))
                .andExpect(status().isBadRequest());
    }
}