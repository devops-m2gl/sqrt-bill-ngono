package com.groupeisi.m2gl.sqrtbillngono.dto;

public class SquareRootRequest {
    private double number;

    public double getNumber () {
        return number;
    }

    public void setNumber(double nbr) {
        if (nbr < 0) {
            throw new IllegalArgumentException("number must be non-negative.");
        }
        this.number = nbr;
    }
}
