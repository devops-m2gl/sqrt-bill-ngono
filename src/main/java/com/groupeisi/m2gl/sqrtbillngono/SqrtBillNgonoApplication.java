package com.groupeisi.m2gl.sqrtbillngono;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SqrtBillNgonoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SqrtBillNgonoApplication.class, args);
    }

}
