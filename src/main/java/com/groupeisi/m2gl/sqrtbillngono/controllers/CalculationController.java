package com.groupeisi.m2gl.sqrtbillngono.controllers;

import com.groupeisi.m2gl.sqrtbillngono.dto.SquareRootRequest;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculationController {

    @PostMapping("/sqrt")
    public double sqrt(@RequestBody SquareRootRequest request) {
        return Math.sqrt(request.getNumber());
    }
}
